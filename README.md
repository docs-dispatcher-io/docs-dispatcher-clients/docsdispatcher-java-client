# docsdispatcher-java-client

Docs Dispatcher API - API document(s) generation
- API version: 3.1.1
  - Build date: 2024-03-23T16:40:16.535479755Z[Etc/UTC]
  - Generator version: 7.5.0-SNAPSHOT

Docs.io API enables you to generate files (currently DOCx, XLSx, PPTx, PDF, HTML, JPG/PNG/SVG) from a template and JSON data. You can just download resulting file, send email with generated attachments or compose services.


*Automatically generated by the [OpenAPI Generator](https://openapi-generator.tech)*


## Requirements

Building the API client library requires:
1. Java 1.8+
2. Maven (3.8.3+)/Gradle (7.2+)

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn clean install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn clean deploy
```

Refer to the [OSSRH Guide](http://central.sonatype.org/pages/ossrh-guide.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
  <groupId>io.docs-dispatcher</groupId>
  <artifactId>docsdispatcher-java-client</artifactId>
  <version>1.0.0</version>
  <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
  repositories {
    mavenCentral()     // Needed if the 'docsdispatcher-java-client' jar has been published to maven central.
    mavenLocal()       // Needed if the 'docsdispatcher-java-client' jar has been published to the local maven repo.
  }

  dependencies {
     implementation "io.docs-dispatcher:docsdispatcher-java-client:1.0.0"
  }
```

### Others

At first generate the JAR by executing:

```shell
mvn clean package
```

Then manually install the following JARs:

* `target/docsdispatcher-java-client-1.0.0.jar`
* `target/lib/*.jar`

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

// Import classes:
import io.docs_dispatcher.client.ApiClient;
import io.docs_dispatcher.client.ApiException;
import io.docs_dispatcher.client.Configuration;
import io.docs_dispatcher.client.models.*;
import io.docs_dispatcher.client.api.ApiStatusApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://api.docs-dispatcher.io");

    ApiStatusApi apiInstance = new ApiStatusApi(defaultClient);
    try {
      Object result = apiInstance.applicationLiveness();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ApiStatusApi#applicationLiveness");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}

```

## Documentation for API Endpoints

All URIs are relative to *https://api.docs-dispatcher.io*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*ApiStatusApi* | [**applicationLiveness**](docs/ApiStatusApi.md#applicationLiveness) | **GET** /healthz | Get API liveness status. If 200, it is running
*ConfigurationsHelpersApi* | [**configurationsGetConfiguration**](docs/ConfigurationsHelpersApi.md#configurationsGetConfiguration) | **GET** /api/configurations/{configTypeName}/config | Get the configuration corresponding to the given config type
*ConfigurationsHelpersApi* | [**configurationsGetConfigurationForProvider**](docs/ConfigurationsHelpersApi.md#configurationsGetConfigurationForProvider) | **GET** /api/configurations/{configTypeName}/providers/{provider}/config | Get the configuration corresponding to the given config type and the given provider
*ConfigurationsHelpersApi* | [**configurationsList**](docs/ConfigurationsHelpersApi.md#configurationsList) | **GET** /api/configurations | Retrieve all configurations json schema for services that are supported by Dispatcher service
*ConfigurationsHelpersApi* | [**configurationsListProviders**](docs/ConfigurationsHelpersApi.md#configurationsListProviders) | **GET** /api/configurations/{configTypeName}/providers | Get the list of implemented providers for the given config type
*ConfigurationsHelpersApi* | [**configurationsValidateConfiguration**](docs/ConfigurationsHelpersApi.md#configurationsValidateConfiguration) | **POST** /api/configurations/{configTypeName}/validate | Validate configuration for the given config type
*EndUserApiApi* | [**dispatchersDispatch**](docs/EndUserApiApi.md#dispatchersDispatch) | **POST** /api/{dispatcherService} | Handle unique service request for file(s) merging or generation
*EndUserApiApi* | [**dispatchersDispatchWithOneComposable**](docs/EndUserApiApi.md#dispatchersDispatchWithOneComposable) | **POST** /api/{dispatcherService}/{composableService1} | Handle multiple services composing calls
*EndUserApiApi* | [**dispatchersDispatchWithTwoComposable**](docs/EndUserApiApi.md#dispatchersDispatchWithTwoComposable) | **POST** /api/{dispatcherService}/{composableService1}/{composableService2} | Handle multiple services composing calls
*EndUserApiApi* | [**dispatchersValidateRequest**](docs/EndUserApiApi.md#dispatchersValidateRequest) | **POST** /api/{dispatcherService}/validate | Validate (JSON) request for the given service
*EndUserApiApi* | [**dispatchersValidateRequestWithOneComposableService**](docs/EndUserApiApi.md#dispatchersValidateRequestWithOneComposableService) | **POST** /api/{dispatcherService}/{composableService1}/validate | Validate (JSON) request for the given service composition
*EndUserApiApi* | [**dispatchersValidateRequestWithTwoComposableServices**](docs/EndUserApiApi.md#dispatchersValidateRequestWithTwoComposableServices) | **POST** /api/{dispatcherService}/{composableService1}/{composableService2}/validate | Validate (JSON) request for the given service composition
*ExternalServicesWebhooksApi* | [**webhooksHandleWebHook**](docs/ExternalServicesWebhooksApi.md#webhooksHandleWebHook) | **POST** /api/webhooks/{dispatcherService}/providers/{provider}/endpoint.json | Handle the external service webhook calls. Service type and provider should be present in the path. Payload should be present in the body and could be anything.
*RequestValidationApi* | [**dispatchersValidateRequest**](docs/RequestValidationApi.md#dispatchersValidateRequest) | **POST** /api/{dispatcherService}/validate | Validate (JSON) request for the given service
*RequestValidationApi* | [**dispatchersValidateRequestWithOneComposableService**](docs/RequestValidationApi.md#dispatchersValidateRequestWithOneComposableService) | **POST** /api/{dispatcherService}/{composableService1}/validate | Validate (JSON) request for the given service composition
*RequestValidationApi* | [**dispatchersValidateRequestWithTwoComposableServices**](docs/RequestValidationApi.md#dispatchersValidateRequestWithTwoComposableServices) | **POST** /api/{dispatcherService}/{composableService1}/{composableService2}/validate | Validate (JSON) request for the given service composition


## Documentation for Models

 - [Address](docs/Address.md)
 - [BasicListRequest](docs/BasicListRequest.md)
 - [BasicRequest](docs/BasicRequest.md)
 - [CompositeRequest](docs/CompositeRequest.md)
 - [ConfigurationElement](docs/ConfigurationElement.md)
 - [ConfigurationSchema](docs/ConfigurationSchema.md)
 - [Creds](docs/Creds.md)
 - [DeferredTargetRequest](docs/DeferredTargetRequest.md)
 - [ESignField](docs/ESignField.md)
 - [ESignFileContentRequest](docs/ESignFileContentRequest.md)
 - [ESignRequest](docs/ESignRequest.md)
 - [EmailRequest](docs/EmailRequest.md)
 - [FileContentRequest](docs/FileContentRequest.md)
 - [MediaFormat](docs/MediaFormat.md)
 - [PostalRequest](docs/PostalRequest.md)
 - [Recipient](docs/Recipient.md)
 - [SMSRequest](docs/SMSRequest.md)
 - [Service](docs/Service.md)
 - [TargetRequest](docs/TargetRequest.md)
 - [Types](docs/Types.md)


<a id="documentation-for-authorization"></a>
## Documentation for Authorization


Authentication schemes defined for the API:
<a id="bearerAuth"></a>
### bearerAuth

- **Type**: HTTP Bearer Token authentication (JWT)


## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author

tech@docs-dispatcher.io

