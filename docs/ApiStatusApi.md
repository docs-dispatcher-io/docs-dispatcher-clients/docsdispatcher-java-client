# ApiStatusApi

All URIs are relative to *https://api.docs-dispatcher.io*

| Method | HTTP request | Description |
|------------- | ------------- | -------------|
| [**applicationLiveness**](ApiStatusApi.md#applicationLiveness) | **GET** /healthz | Get API liveness status. If 200, it is running |


<a id="applicationLiveness"></a>
# **applicationLiveness**
> Object applicationLiveness()

Get API liveness status. If 200, it is running

### Example
```java
// Import classes:
import io.docs_dispatcher.client.ApiClient;
import io.docs_dispatcher.client.ApiException;
import io.docs_dispatcher.client.Configuration;
import io.docs_dispatcher.client.models.*;
import io.docs_dispatcher.client.api.ApiStatusApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://api.docs-dispatcher.io");

    ApiStatusApi apiInstance = new ApiStatusApi(defaultClient);
    try {
      Object result = apiInstance.applicationLiveness();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ApiStatusApi#applicationLiveness");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Empty success response |  -  |

