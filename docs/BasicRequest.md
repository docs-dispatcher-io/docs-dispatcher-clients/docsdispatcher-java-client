

# BasicRequest


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**targets** | [**List&lt;TargetRequest&gt;**](TargetRequest.md) |  |  [optional] |
|**templateName** | **String** |  |  [optional] |
|**data** | **Object** |  |  [optional] |
|**resultFileName** | **String** |  |  [optional] |



