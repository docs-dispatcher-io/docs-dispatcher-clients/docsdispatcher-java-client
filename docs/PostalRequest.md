

# PostalRequest


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**provider** | **String** |  |  [optional] |
|**subject** | **String** |  |  [optional] |
|**sender** | [**Address**](Address.md) |  |  [optional] |
|**receivers** | [**List&lt;Address&gt;**](Address.md) |  |  [optional] |
|**documents** | [**List&lt;FileContentRequest&gt;**](FileContentRequest.md) |  |  [optional] |
|**data** | **Object** |  |  [optional] |
|**envelopeFormat** | [**EnvelopeFormatEnum**](#EnvelopeFormatEnum) |  |  [optional] |
|**colorMode** | [**ColorModeEnum**](#ColorModeEnum) |  |  [optional] |
|**postage** | [**PostageEnum**](#PostageEnum) |  |  [optional] |
|**bothSides** | **Boolean** |  |  [optional] |
|**targetFilename** | **String** |  |  [optional] |
|**settings** | **Object** |  |  [optional] |
|**targets** | [**List&lt;DeferredTargetRequest&gt;**](DeferredTargetRequest.md) |  |  [optional] |



## Enum: EnvelopeFormatEnum

| Name | Value |
|---- | -----|
| C4 | &quot;C4&quot; |
| C5 | &quot;C5&quot; |
| C6 | &quot;C6&quot; |
| AUTO | &quot;AUTO&quot; |



## Enum: ColorModeEnum

| Name | Value |
|---- | -----|
| COLOR | &quot;COLOR&quot; |
| BW | &quot;BW&quot; |



## Enum: PostageEnum

| Name | Value |
|---- | -----|
| ECO | &quot;ECO&quot; |
| REGISTERED_LETTER | &quot;REGISTERED_LETTER&quot; |
| REGISTERED_LETTER_WITH_ACK | &quot;REGISTERED_LETTER_WITH_ACK&quot; |
| FAST | &quot;FAST&quot; |
| FAST_TRACKED | &quot;FAST_TRACKED&quot; |
| SLOW | &quot;SLOW&quot; |
| SLOW_TRACKED | &quot;SLOW_TRACKED&quot; |



