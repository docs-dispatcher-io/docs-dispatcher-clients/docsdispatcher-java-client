

# Creds


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**username** | **String** |  |  [optional] |
|**password** | **String** |  |  [optional] |
|**scheme** | [**SchemeEnum**](#SchemeEnum) |  |  [optional] |
|**companyId** | **String** |  |  [optional] |
|**userId** | **String** |  |  [optional] |
|**bearer** | **String** |  |  [optional] |



## Enum: SchemeEnum

| Name | Value |
|---- | -----|
| DIGEST | &quot;DIGEST&quot; |
| BASIC | &quot;BASIC&quot; |
| NTLM | &quot;NTLM&quot; |
| SPNEGO | &quot;SPNEGO&quot; |
| KERBEROS | &quot;KERBEROS&quot; |
| NONE | &quot;NONE&quot; |



