

# CompositeRequest


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**targets** | [**List&lt;TargetRequest&gt;**](TargetRequest.md) |  |  [optional] |
|**templateName** | **String** |  |  [optional] |
|**data** | **Object** |  |  [optional] |
|**resultFileName** | **String** |  |  [optional] |
|**body** | **String** |  |  [optional] |
|**subject** | **String** |  |  [optional] |
|**from** | **String** |  |  [optional] |
|**to** | **List&lt;String&gt;** |  |  [optional] |
|**cc** | **List&lt;String&gt;** |  |  [optional] |
|**bcc** | **List&lt;String&gt;** |  |  [optional] |
|**attachments** | [**List&lt;FileContentRequest&gt;**](FileContentRequest.md) |  |  [optional] |
|**provider** | **String** |  |  [optional] |
|**sender** | [**Address**](Address.md) |  |  [optional] |
|**receivers** | [**List&lt;Address&gt;**](Address.md) |  |  [optional] |
|**documents** | [**List&lt;ESignFileContentRequest&gt;**](ESignFileContentRequest.md) |  |  [optional] |
|**envelopeFormat** | [**EnvelopeFormatEnum**](#EnvelopeFormatEnum) |  |  [optional] |
|**colorMode** | [**ColorModeEnum**](#ColorModeEnum) |  |  [optional] |
|**postage** | [**PostageEnum**](#PostageEnum) |  |  [optional] |
|**bothSides** | **Boolean** |  |  [optional] |
|**targetFilename** | **String** |  |  [optional] |
|**settings** | **Object** |  |  [optional] |
|**messageBody** | **String** |  |  [optional] |
|**message** | [**BasicRequest**](BasicRequest.md) |  |  [optional] |
|**finalDocMessageSubject** | **String** |  |  [optional] |
|**finalDocMessageBody** | **String** |  |  [optional] |
|**finalDocMessage** | [**BasicRequest**](BasicRequest.md) |  |  [optional] |
|**recipients** | [**List&lt;Recipient&gt;**](Recipient.md) |  |  [optional] |
|**finalDocRecipients** | [**List&lt;Recipient&gt;**](Recipient.md) |  |  [optional] |
|**type** | [**TypeEnum**](#TypeEnum) |  |  [optional] |
|**deliveryType** | [**DeliveryTypeEnum**](#DeliveryTypeEnum) |  |  [optional] |
|**signingMode** | [**SigningModeEnum**](#SigningModeEnum) |  |  [optional] |
|**expirationTime** | **Integer** |  |  [optional] |
|**fields** | [**List&lt;ESignField&gt;**](ESignField.md) |  |  [optional] |
|**providerName** | **String** |  |  [optional] |
|**smsContent** | **String** |  |  [optional] |
|**requests** | [**List&lt;BasicRequest&gt;**](BasicRequest.md) |  |  [optional] |



## Enum: EnvelopeFormatEnum

| Name | Value |
|---- | -----|
| C4 | &quot;C4&quot; |
| C5 | &quot;C5&quot; |
| C6 | &quot;C6&quot; |
| AUTO | &quot;AUTO&quot; |



## Enum: ColorModeEnum

| Name | Value |
|---- | -----|
| COLOR | &quot;COLOR&quot; |
| BW | &quot;BW&quot; |



## Enum: PostageEnum

| Name | Value |
|---- | -----|
| ECO | &quot;ECO&quot; |
| REGISTERED_LETTER | &quot;REGISTERED_LETTER&quot; |
| REGISTERED_LETTER_WITH_ACK | &quot;REGISTERED_LETTER_WITH_ACK&quot; |
| FAST | &quot;FAST&quot; |
| FAST_TRACKED | &quot;FAST_TRACKED&quot; |
| SLOW | &quot;SLOW&quot; |
| SLOW_TRACKED | &quot;SLOW_TRACKED&quot; |



## Enum: TypeEnum

| Name | Value |
|---- | -----|
| SIMPLE | &quot;SIMPLE&quot; |
| CERTIFIED | &quot;CERTIFIED&quot; |
| SMART | &quot;SMART&quot; |
| ADVANCED | &quot;ADVANCED&quot; |



## Enum: DeliveryTypeEnum

| Name | Value |
|---- | -----|
| EMAIL | &quot;EMAIL&quot; |
| URL | &quot;URL&quot; |
| SMS | &quot;SMS&quot; |
| WEB | &quot;WEB&quot; |



## Enum: SigningModeEnum

| Name | Value |
|---- | -----|
| SEQUENTIAL | &quot;SEQUENTIAL&quot; |
| PARALLEL | &quot;PARALLEL&quot; |



