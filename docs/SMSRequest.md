

# SMSRequest


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**providerName** | **String** |  |  [optional] |
|**smsContent** | **String** |  |  [optional] |
|**templateName** | **String** |  |  [optional] |
|**from** | **String** |  |  [optional] |
|**to** | **List&lt;String&gt;** |  |  [optional] |
|**data** | **Object** |  |  [optional] |



