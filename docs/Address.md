

# Address

Specific object to format address fields

## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**name** | **String** | Postal address field for person name or company |  |
|**address1** | **String** | Postal address field for address&#39; line 1 |  |
|**address2** | **String** | Postal address field for address&#39; line 2 |  [optional] |
|**address3** | **String** | Postal address field for address&#39; line 3 |  [optional] |
|**address4** | **String** | Postal address field for address&#39; line 4 |  [optional] |
|**city** | **String** | Postal address field for city |  |
|**zipCode** | **String** | Postal address field for zipcode |  |
|**countryCode** | **String** | Postal address field for country as a 2 characters ISO 3166 format |  |



