

# TargetRequest


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**target** | [**TargetEnum**](#TargetEnum) |  |  [optional] |
|**id** | **String** |  |  [optional] |
|**type** | **String** |  |  [optional] |



## Enum: TargetEnum

| Name | Value |
|---- | -----|
| ZOHOCRMUPLOAD | &quot;zohoCRMUpload&quot; |
| GEMAUPLOAD | &quot;gemaUpload&quot; |



