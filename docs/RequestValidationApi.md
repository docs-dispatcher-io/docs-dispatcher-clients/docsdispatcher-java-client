# RequestValidationApi

All URIs are relative to *https://api.docs-dispatcher.io*

| Method | HTTP request | Description |
|------------- | ------------- | -------------|
| [**dispatchersValidateRequest**](RequestValidationApi.md#dispatchersValidateRequest) | **POST** /api/{dispatcherService}/validate | Validate (JSON) request for the given service |
| [**dispatchersValidateRequestWithOneComposableService**](RequestValidationApi.md#dispatchersValidateRequestWithOneComposableService) | **POST** /api/{dispatcherService}/{composableService1}/validate | Validate (JSON) request for the given service composition |
| [**dispatchersValidateRequestWithTwoComposableServices**](RequestValidationApi.md#dispatchersValidateRequestWithTwoComposableServices) | **POST** /api/{dispatcherService}/{composableService1}/{composableService2}/validate | Validate (JSON) request for the given service composition |


<a id="dispatchersValidateRequest"></a>
# **dispatchersValidateRequest**
> dispatchersValidateRequest(dispatcherService, compositeRequest)

Validate (JSON) request for the given service

### Example
```java
// Import classes:
import io.docs_dispatcher.client.ApiClient;
import io.docs_dispatcher.client.ApiException;
import io.docs_dispatcher.client.Configuration;
import io.docs_dispatcher.client.auth.*;
import io.docs_dispatcher.client.models.*;
import io.docs_dispatcher.client.api.RequestValidationApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://api.docs-dispatcher.io");
    
    // Configure HTTP bearer authorization: bearerAuth
    HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setBearerToken("BEARER TOKEN");

    RequestValidationApi apiInstance = new RequestValidationApi(defaultClient);
    Service dispatcherService = Service.fromValue("email"); // Service | The service used to handle the request
    CompositeRequest compositeRequest = new CompositeRequest(); // CompositeRequest | 
    try {
      apiInstance.dispatchersValidateRequest(dispatcherService, compositeRequest);
    } catch (ApiException e) {
      System.err.println("Exception when calling RequestValidationApi#dispatchersValidateRequest");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **dispatcherService** | **Service**| The service used to handle the request | [enum: email, file, sms, postal, upload, esign] |
| **compositeRequest** | [**CompositeRequest**](CompositeRequest.md)|  | [optional] |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **400** | for ex if the data object is not correct or missing |  -  |
| **401** | Unauthorized call to service (distant or this API) |  -  |
| **403** | Forbidden call to service (distant or this API) |  -  |
| **415** | Accept header media type missing or wildcard given |  -  |
| **417** | For the given service, additional parameters are required and not present |  -  |
| **501** | Service or method requested is not yet implemented |  -  |
| **200** | Request is valid |  -  |

<a id="dispatchersValidateRequestWithOneComposableService"></a>
# **dispatchersValidateRequestWithOneComposableService**
> dispatchersValidateRequestWithOneComposableService(dispatcherService, composableService1, compositeRequest)

Validate (JSON) request for the given service composition

### Example
```java
// Import classes:
import io.docs_dispatcher.client.ApiClient;
import io.docs_dispatcher.client.ApiException;
import io.docs_dispatcher.client.Configuration;
import io.docs_dispatcher.client.auth.*;
import io.docs_dispatcher.client.models.*;
import io.docs_dispatcher.client.api.RequestValidationApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://api.docs-dispatcher.io");
    
    // Configure HTTP bearer authorization: bearerAuth
    HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setBearerToken("BEARER TOKEN");

    RequestValidationApi apiInstance = new RequestValidationApi(defaultClient);
    Service dispatcherService = Service.fromValue("email"); // Service | The service used to handle the request
    Service composableService1 = Service.fromValue("email"); // Service | A service that can be composed with result of the first one
    CompositeRequest compositeRequest = new CompositeRequest(); // CompositeRequest | 
    try {
      apiInstance.dispatchersValidateRequestWithOneComposableService(dispatcherService, composableService1, compositeRequest);
    } catch (ApiException e) {
      System.err.println("Exception when calling RequestValidationApi#dispatchersValidateRequestWithOneComposableService");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **dispatcherService** | **Service**| The service used to handle the request | [enum: email, file, sms, postal, upload, esign] |
| **composableService1** | **Service**| A service that can be composed with result of the first one | [enum: email, file, sms, postal, upload, esign] |
| **compositeRequest** | [**CompositeRequest**](CompositeRequest.md)|  | [optional] |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **400** | for ex if the data object is not correct or missing |  -  |
| **401** | Unauthorized call to service (distant or this API) |  -  |
| **403** | Forbidden call to service (distant or this API) |  -  |
| **415** | Accept header media type missing or wildcard given |  -  |
| **417** | For the given service, additional parameters are required and not present |  -  |
| **501** | Service or method requested is not yet implemented |  -  |
| **200** | Request is valid |  -  |

<a id="dispatchersValidateRequestWithTwoComposableServices"></a>
# **dispatchersValidateRequestWithTwoComposableServices**
> dispatchersValidateRequestWithTwoComposableServices(dispatcherService, composableService1, composableService2, compositeRequest)

Validate (JSON) request for the given service composition

### Example
```java
// Import classes:
import io.docs_dispatcher.client.ApiClient;
import io.docs_dispatcher.client.ApiException;
import io.docs_dispatcher.client.Configuration;
import io.docs_dispatcher.client.auth.*;
import io.docs_dispatcher.client.models.*;
import io.docs_dispatcher.client.api.RequestValidationApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://api.docs-dispatcher.io");
    
    // Configure HTTP bearer authorization: bearerAuth
    HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setBearerToken("BEARER TOKEN");

    RequestValidationApi apiInstance = new RequestValidationApi(defaultClient);
    Service dispatcherService = Service.fromValue("email"); // Service | The service used to handle the request
    Service composableService1 = Service.fromValue("email"); // Service | A service that can be composed with result of the first one
    Service composableService2 = Service.fromValue("email"); // Service | A service that can be composed with result of the first one
    CompositeRequest compositeRequest = new CompositeRequest(); // CompositeRequest | 
    try {
      apiInstance.dispatchersValidateRequestWithTwoComposableServices(dispatcherService, composableService1, composableService2, compositeRequest);
    } catch (ApiException e) {
      System.err.println("Exception when calling RequestValidationApi#dispatchersValidateRequestWithTwoComposableServices");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **dispatcherService** | **Service**| The service used to handle the request | [enum: email, file, sms, postal, upload, esign] |
| **composableService1** | **Service**| A service that can be composed with result of the first one | [enum: email, file, sms, postal, upload, esign] |
| **composableService2** | **Service**| A service that can be composed with result of the first one | [enum: email, file, sms, postal, upload, esign] |
| **compositeRequest** | [**CompositeRequest**](CompositeRequest.md)|  | [optional] |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **400** | for ex if the data object is not correct or missing |  -  |
| **401** | Unauthorized call to service (distant or this API) |  -  |
| **403** | Forbidden call to service (distant or this API) |  -  |
| **415** | Accept header media type missing or wildcard given |  -  |
| **417** | For the given service, additional parameters are required and not present |  -  |
| **501** | Service or method requested is not yet implemented |  -  |
| **200** | Request is valid |  -  |

