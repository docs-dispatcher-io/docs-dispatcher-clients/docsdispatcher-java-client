# ExternalServicesWebhooksApi

All URIs are relative to *https://api.docs-dispatcher.io*

| Method | HTTP request | Description |
|------------- | ------------- | -------------|
| [**webhooksHandleWebHook**](ExternalServicesWebhooksApi.md#webhooksHandleWebHook) | **POST** /api/webhooks/{dispatcherService}/providers/{provider}/endpoint.json | Handle the external service webhook calls. Service type and provider should be present in the path. Payload should be present in the body and could be anything. |


<a id="webhooksHandleWebHook"></a>
# **webhooksHandleWebHook**
> Object webhooksHandleWebHook(dispatcherService, provider, body)

Handle the external service webhook calls. Service type and provider should be present in the path. Payload should be present in the body and could be anything.

### Example
```java
// Import classes:
import io.docs_dispatcher.client.ApiClient;
import io.docs_dispatcher.client.ApiException;
import io.docs_dispatcher.client.Configuration;
import io.docs_dispatcher.client.auth.*;
import io.docs_dispatcher.client.models.*;
import io.docs_dispatcher.client.api.ExternalServicesWebhooksApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://api.docs-dispatcher.io");
    
    // Configure HTTP bearer authorization: bearerAuth
    HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setBearerToken("BEARER TOKEN");

    ExternalServicesWebhooksApi apiInstance = new ExternalServicesWebhooksApi(defaultClient);
    Service dispatcherService = Service.fromValue("email"); // Service | The service used to handle the request
    String provider = "provider_example"; // String | The provider name from which the callback comes.
    Object body = null; // Object | 
    try {
      Object result = apiInstance.webhooksHandleWebHook(dispatcherService, provider, body);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ExternalServicesWebhooksApi#webhooksHandleWebHook");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **dispatcherService** | **Service**| The service used to handle the request | [enum: email, file, sms, postal, upload, esign] |
| **provider** | **String**| The provider name from which the callback comes. | |
| **body** | **Object**|  | [optional] |

### Return type

**Object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain, application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **501** | Service or method requested is not yet implemented |  -  |
| **200** | Empty success response |  -  |

