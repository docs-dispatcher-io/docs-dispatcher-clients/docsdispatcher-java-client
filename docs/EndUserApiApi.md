# EndUserApiApi

All URIs are relative to *https://api.docs-dispatcher.io*

| Method | HTTP request | Description |
|------------- | ------------- | -------------|
| [**dispatchersDispatch**](EndUserApiApi.md#dispatchersDispatch) | **POST** /api/{dispatcherService} | Handle unique service request for file(s) merging or generation |
| [**dispatchersDispatchWithOneComposable**](EndUserApiApi.md#dispatchersDispatchWithOneComposable) | **POST** /api/{dispatcherService}/{composableService1} | Handle multiple services composing calls |
| [**dispatchersDispatchWithTwoComposable**](EndUserApiApi.md#dispatchersDispatchWithTwoComposable) | **POST** /api/{dispatcherService}/{composableService1}/{composableService2} | Handle multiple services composing calls |
| [**dispatchersValidateRequest**](EndUserApiApi.md#dispatchersValidateRequest) | **POST** /api/{dispatcherService}/validate | Validate (JSON) request for the given service |
| [**dispatchersValidateRequestWithOneComposableService**](EndUserApiApi.md#dispatchersValidateRequestWithOneComposableService) | **POST** /api/{dispatcherService}/{composableService1}/validate | Validate (JSON) request for the given service composition |
| [**dispatchersValidateRequestWithTwoComposableServices**](EndUserApiApi.md#dispatchersValidateRequestWithTwoComposableServices) | **POST** /api/{dispatcherService}/{composableService1}/{composableService2}/validate | Validate (JSON) request for the given service composition |


<a id="dispatchersDispatch"></a>
# **dispatchersDispatch**
> File dispatchersDispatch(dispatcherService, compositeRequest)

Handle unique service request for file(s) merging or generation

### Example
```java
// Import classes:
import io.docs_dispatcher.client.ApiClient;
import io.docs_dispatcher.client.ApiException;
import io.docs_dispatcher.client.Configuration;
import io.docs_dispatcher.client.auth.*;
import io.docs_dispatcher.client.models.*;
import io.docs_dispatcher.client.api.EndUserApiApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://api.docs-dispatcher.io");
    
    // Configure HTTP bearer authorization: bearerAuth
    HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setBearerToken("BEARER TOKEN");

    EndUserApiApi apiInstance = new EndUserApiApi(defaultClient);
    Service dispatcherService = Service.fromValue("email"); // Service | The service used to handle the request
    CompositeRequest compositeRequest = new CompositeRequest(); // CompositeRequest | 
    try {
      File result = apiInstance.dispatchersDispatch(dispatcherService, compositeRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling EndUserApiApi#dispatchersDispatch");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **dispatcherService** | **Service**| The service used to handle the request | [enum: email, file, sms, postal, upload, esign] |
| **compositeRequest** | [**CompositeRequest**](CompositeRequest.md)|  | [optional] |

### Return type

[**File**](File.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain, text/html, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.openxmlformats-officedocument.presentationml.presentation, image/svg+xml, image/png, image/jpeg, application/pdf, multipart/alternative

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **400** | for ex if the data object is not correct or missing |  -  |
| **401** | Unauthorized call to service (distant or this API) |  -  |
| **403** | Forbidden call to service (distant or this API) |  -  |
| **404** | A distant service response, for ex: Template name unknown, client configuration not found... |  -  |
| **415** | Accept header media type missing or wildcard given |  -  |
| **417** | For the given service, additional parameters are required and not present |  -  |
| **500** | Distant service throws unexpected error |  -  |
| **501** | Service or method requested is not yet implemented |  -  |
| **200** | Successful generation. The result file returned as inline attachment |  -  |

<a id="dispatchersDispatchWithOneComposable"></a>
# **dispatchersDispatchWithOneComposable**
> File dispatchersDispatchWithOneComposable(dispatcherService, composableService1, compositeRequest)

Handle multiple services composing calls

### Example
```java
// Import classes:
import io.docs_dispatcher.client.ApiClient;
import io.docs_dispatcher.client.ApiException;
import io.docs_dispatcher.client.Configuration;
import io.docs_dispatcher.client.auth.*;
import io.docs_dispatcher.client.models.*;
import io.docs_dispatcher.client.api.EndUserApiApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://api.docs-dispatcher.io");
    
    // Configure HTTP bearer authorization: bearerAuth
    HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setBearerToken("BEARER TOKEN");

    EndUserApiApi apiInstance = new EndUserApiApi(defaultClient);
    Service dispatcherService = Service.fromValue("email"); // Service | The service used to handle the request
    Service composableService1 = Service.fromValue("email"); // Service | A service that can be composed with result of the first one
    CompositeRequest compositeRequest = new CompositeRequest(); // CompositeRequest | 
    try {
      File result = apiInstance.dispatchersDispatchWithOneComposable(dispatcherService, composableService1, compositeRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling EndUserApiApi#dispatchersDispatchWithOneComposable");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **dispatcherService** | **Service**| The service used to handle the request | [enum: email, file, sms, postal, upload, esign] |
| **composableService1** | **Service**| A service that can be composed with result of the first one | [enum: email, file, sms, postal, upload, esign] |
| **compositeRequest** | [**CompositeRequest**](CompositeRequest.md)|  | [optional] |

### Return type

[**File**](File.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain, text/html, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.openxmlformats-officedocument.presentationml.presentation, image/svg+xml, image/png, image/jpeg, application/pdf, multipart/alternative

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **400** | for ex if the data object is not correct or missing |  -  |
| **401** | Unauthorized call to service (distant or this API) |  -  |
| **403** | Forbidden call to service (distant or this API) |  -  |
| **404** | A distant service response, for ex: Template name unknown, client configuration not found... |  -  |
| **415** | Accept header media type missing or wildcard given |  -  |
| **417** | For the given service, additional parameters are required and not present |  -  |
| **500** | Distant service throws unexpected error |  -  |
| **501** | for ex: the data object is not correct or missing, one of the specified service does exist but is not usable like requested |  -  |
| **200** | Successful generation. The result file returned as inline attachment |  -  |

<a id="dispatchersDispatchWithTwoComposable"></a>
# **dispatchersDispatchWithTwoComposable**
> File dispatchersDispatchWithTwoComposable(dispatcherService, composableService1, composableService2, compositeRequest)

Handle multiple services composing calls

### Example
```java
// Import classes:
import io.docs_dispatcher.client.ApiClient;
import io.docs_dispatcher.client.ApiException;
import io.docs_dispatcher.client.Configuration;
import io.docs_dispatcher.client.auth.*;
import io.docs_dispatcher.client.models.*;
import io.docs_dispatcher.client.api.EndUserApiApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://api.docs-dispatcher.io");
    
    // Configure HTTP bearer authorization: bearerAuth
    HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setBearerToken("BEARER TOKEN");

    EndUserApiApi apiInstance = new EndUserApiApi(defaultClient);
    Service dispatcherService = Service.fromValue("email"); // Service | The service used to handle the request
    Service composableService1 = Service.fromValue("email"); // Service | A service that can be composed with result of the first one
    Service composableService2 = Service.fromValue("email"); // Service | A service that can be composed with result of the first one
    CompositeRequest compositeRequest = new CompositeRequest(); // CompositeRequest | 
    try {
      File result = apiInstance.dispatchersDispatchWithTwoComposable(dispatcherService, composableService1, composableService2, compositeRequest);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling EndUserApiApi#dispatchersDispatchWithTwoComposable");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **dispatcherService** | **Service**| The service used to handle the request | [enum: email, file, sms, postal, upload, esign] |
| **composableService1** | **Service**| A service that can be composed with result of the first one | [enum: email, file, sms, postal, upload, esign] |
| **composableService2** | **Service**| A service that can be composed with result of the first one | [enum: email, file, sms, postal, upload, esign] |
| **compositeRequest** | [**CompositeRequest**](CompositeRequest.md)|  | [optional] |

### Return type

[**File**](File.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain, text/html, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.openxmlformats-officedocument.presentationml.presentation, image/svg+xml, image/png, image/jpeg, application/pdf, multipart/alternative

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **400** | for ex if the data object is not correct or missing |  -  |
| **401** | Unauthorized call to service (distant or this API) |  -  |
| **403** | Forbidden call to service (distant or this API) |  -  |
| **404** | A distant service response, for ex: Template name unknown, client configuration not found... |  -  |
| **415** | Accept header media type missing or wildcard given |  -  |
| **417** | For the given service, additional parameters are required and not present |  -  |
| **500** | Distant service throws unexpected error |  -  |
| **501** | for ex: the data object is not correct or missing, one of the specified service does exist but is not usable like requested |  -  |
| **200** | Successful generation. The result file returned as inline attachment |  -  |

<a id="dispatchersValidateRequest"></a>
# **dispatchersValidateRequest**
> dispatchersValidateRequest(dispatcherService, compositeRequest)

Validate (JSON) request for the given service

### Example
```java
// Import classes:
import io.docs_dispatcher.client.ApiClient;
import io.docs_dispatcher.client.ApiException;
import io.docs_dispatcher.client.Configuration;
import io.docs_dispatcher.client.auth.*;
import io.docs_dispatcher.client.models.*;
import io.docs_dispatcher.client.api.EndUserApiApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://api.docs-dispatcher.io");
    
    // Configure HTTP bearer authorization: bearerAuth
    HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setBearerToken("BEARER TOKEN");

    EndUserApiApi apiInstance = new EndUserApiApi(defaultClient);
    Service dispatcherService = Service.fromValue("email"); // Service | The service used to handle the request
    CompositeRequest compositeRequest = new CompositeRequest(); // CompositeRequest | 
    try {
      apiInstance.dispatchersValidateRequest(dispatcherService, compositeRequest);
    } catch (ApiException e) {
      System.err.println("Exception when calling EndUserApiApi#dispatchersValidateRequest");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **dispatcherService** | **Service**| The service used to handle the request | [enum: email, file, sms, postal, upload, esign] |
| **compositeRequest** | [**CompositeRequest**](CompositeRequest.md)|  | [optional] |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **400** | for ex if the data object is not correct or missing |  -  |
| **401** | Unauthorized call to service (distant or this API) |  -  |
| **403** | Forbidden call to service (distant or this API) |  -  |
| **415** | Accept header media type missing or wildcard given |  -  |
| **417** | For the given service, additional parameters are required and not present |  -  |
| **501** | Service or method requested is not yet implemented |  -  |
| **200** | Request is valid |  -  |

<a id="dispatchersValidateRequestWithOneComposableService"></a>
# **dispatchersValidateRequestWithOneComposableService**
> dispatchersValidateRequestWithOneComposableService(dispatcherService, composableService1, compositeRequest)

Validate (JSON) request for the given service composition

### Example
```java
// Import classes:
import io.docs_dispatcher.client.ApiClient;
import io.docs_dispatcher.client.ApiException;
import io.docs_dispatcher.client.Configuration;
import io.docs_dispatcher.client.auth.*;
import io.docs_dispatcher.client.models.*;
import io.docs_dispatcher.client.api.EndUserApiApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://api.docs-dispatcher.io");
    
    // Configure HTTP bearer authorization: bearerAuth
    HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setBearerToken("BEARER TOKEN");

    EndUserApiApi apiInstance = new EndUserApiApi(defaultClient);
    Service dispatcherService = Service.fromValue("email"); // Service | The service used to handle the request
    Service composableService1 = Service.fromValue("email"); // Service | A service that can be composed with result of the first one
    CompositeRequest compositeRequest = new CompositeRequest(); // CompositeRequest | 
    try {
      apiInstance.dispatchersValidateRequestWithOneComposableService(dispatcherService, composableService1, compositeRequest);
    } catch (ApiException e) {
      System.err.println("Exception when calling EndUserApiApi#dispatchersValidateRequestWithOneComposableService");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **dispatcherService** | **Service**| The service used to handle the request | [enum: email, file, sms, postal, upload, esign] |
| **composableService1** | **Service**| A service that can be composed with result of the first one | [enum: email, file, sms, postal, upload, esign] |
| **compositeRequest** | [**CompositeRequest**](CompositeRequest.md)|  | [optional] |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **400** | for ex if the data object is not correct or missing |  -  |
| **401** | Unauthorized call to service (distant or this API) |  -  |
| **403** | Forbidden call to service (distant or this API) |  -  |
| **415** | Accept header media type missing or wildcard given |  -  |
| **417** | For the given service, additional parameters are required and not present |  -  |
| **501** | Service or method requested is not yet implemented |  -  |
| **200** | Request is valid |  -  |

<a id="dispatchersValidateRequestWithTwoComposableServices"></a>
# **dispatchersValidateRequestWithTwoComposableServices**
> dispatchersValidateRequestWithTwoComposableServices(dispatcherService, composableService1, composableService2, compositeRequest)

Validate (JSON) request for the given service composition

### Example
```java
// Import classes:
import io.docs_dispatcher.client.ApiClient;
import io.docs_dispatcher.client.ApiException;
import io.docs_dispatcher.client.Configuration;
import io.docs_dispatcher.client.auth.*;
import io.docs_dispatcher.client.models.*;
import io.docs_dispatcher.client.api.EndUserApiApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://api.docs-dispatcher.io");
    
    // Configure HTTP bearer authorization: bearerAuth
    HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setBearerToken("BEARER TOKEN");

    EndUserApiApi apiInstance = new EndUserApiApi(defaultClient);
    Service dispatcherService = Service.fromValue("email"); // Service | The service used to handle the request
    Service composableService1 = Service.fromValue("email"); // Service | A service that can be composed with result of the first one
    Service composableService2 = Service.fromValue("email"); // Service | A service that can be composed with result of the first one
    CompositeRequest compositeRequest = new CompositeRequest(); // CompositeRequest | 
    try {
      apiInstance.dispatchersValidateRequestWithTwoComposableServices(dispatcherService, composableService1, composableService2, compositeRequest);
    } catch (ApiException e) {
      System.err.println("Exception when calling EndUserApiApi#dispatchersValidateRequestWithTwoComposableServices");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **dispatcherService** | **Service**| The service used to handle the request | [enum: email, file, sms, postal, upload, esign] |
| **composableService1** | **Service**| A service that can be composed with result of the first one | [enum: email, file, sms, postal, upload, esign] |
| **composableService2** | **Service**| A service that can be composed with result of the first one | [enum: email, file, sms, postal, upload, esign] |
| **compositeRequest** | [**CompositeRequest**](CompositeRequest.md)|  | [optional] |

### Return type

null (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **400** | for ex if the data object is not correct or missing |  -  |
| **401** | Unauthorized call to service (distant or this API) |  -  |
| **403** | Forbidden call to service (distant or this API) |  -  |
| **415** | Accept header media type missing or wildcard given |  -  |
| **417** | For the given service, additional parameters are required and not present |  -  |
| **501** | Service or method requested is not yet implemented |  -  |
| **200** | Request is valid |  -  |

