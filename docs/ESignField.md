

# ESignField

Specific object to format anchors that have to be parsed into documents for example to display user's signature

## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**recipient** | **String** | Recipient field for internal identifier. Usefull for request scoped references. |  |
|**anchorKey** | **String** | The key for building the json path to that anchor so as the document can be generated with the anchor. |  [optional] |
|**type** | [**TypeEnum**](#TypeEnum) | The anchor type. Usefull only for for building the json path to that anchor so as the document can be generated with the anchor. |  [optional] |
|**additionalOptions** | **String** | The anchor optional additional settings to be printed into the document |  [optional] |
|**consents** | **List&lt;String&gt;** | List of anchor associated consents to display to the user |  [optional] |



## Enum: TypeEnum

| Name | Value |
|---- | -----|
| SIGNATURE | &quot;SIGNATURE&quot; |
| VISA | &quot;VISA&quot; |



