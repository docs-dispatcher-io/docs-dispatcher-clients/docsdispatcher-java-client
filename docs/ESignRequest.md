

# ESignRequest


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**provider** | **String** |  |  [optional] |
|**subject** | **String** |  |  [optional] |
|**messageBody** | **String** |  |  [optional] |
|**message** | [**BasicRequest**](BasicRequest.md) |  |  [optional] |
|**finalDocMessageSubject** | **String** |  |  [optional] |
|**finalDocMessageBody** | **String** |  |  [optional] |
|**finalDocMessage** | [**BasicRequest**](BasicRequest.md) |  |  [optional] |
|**recipients** | [**List&lt;Recipient&gt;**](Recipient.md) |  |  [optional] |
|**documents** | [**List&lt;ESignFileContentRequest&gt;**](ESignFileContentRequest.md) |  |  [optional] |
|**finalDocRecipients** | [**List&lt;Recipient&gt;**](Recipient.md) |  |  [optional] |
|**data** | **Object** |  |  [optional] |
|**type** | [**TypeEnum**](#TypeEnum) |  |  [optional] |
|**deliveryType** | [**DeliveryTypeEnum**](#DeliveryTypeEnum) |  |  [optional] |
|**signingMode** | [**SigningModeEnum**](#SigningModeEnum) |  |  [optional] |
|**expirationTime** | **Integer** |  |  [optional] |
|**fields** | [**List&lt;ESignField&gt;**](ESignField.md) |  |  [optional] |
|**settings** | **Object** |  |  [optional] |
|**targets** | [**List&lt;DeferredTargetRequest&gt;**](DeferredTargetRequest.md) |  |  [optional] |



## Enum: TypeEnum

| Name | Value |
|---- | -----|
| SIMPLE | &quot;SIMPLE&quot; |
| CERTIFIED | &quot;CERTIFIED&quot; |
| SMART | &quot;SMART&quot; |
| ADVANCED | &quot;ADVANCED&quot; |



## Enum: DeliveryTypeEnum

| Name | Value |
|---- | -----|
| EMAIL | &quot;EMAIL&quot; |
| URL | &quot;URL&quot; |
| SMS | &quot;SMS&quot; |
| WEB | &quot;WEB&quot; |



## Enum: SigningModeEnum

| Name | Value |
|---- | -----|
| SEQUENTIAL | &quot;SEQUENTIAL&quot; |
| PARALLEL | &quot;PARALLEL&quot; |



