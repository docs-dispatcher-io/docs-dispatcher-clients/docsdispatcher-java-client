

# ConfigurationElement


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**id** | **Integer** |  |  [optional] |
|**creator** | **Integer** |  |  |
|**creationDate** | **Object** |  |  [optional] |
|**lastModifier** | **Integer** |  |  |
|**lastModificationDate** | **Object** |  |  [optional] |
|**deleted** | **Boolean** |  |  |
|**name** | **String** |  |  [optional] |
|**type** | [**TypeEnum**](#TypeEnum) |  |  [optional] |
|**providerName** | **String** |  |  [optional] |
|**isDefault** | **Boolean** |  |  |
|**configContent** | **Object** |  |  [optional] |



## Enum: TypeEnum

| Name | Value |
|---- | -----|
| SMTP | &quot;SMTP&quot; |
| SMS | &quot;SMS&quot; |
| POSTAL | &quot;POSTAL&quot; |
| E_SIGN | &quot;E_SIGN&quot; |
| UPLOAD | &quot;UPLOAD&quot; |



