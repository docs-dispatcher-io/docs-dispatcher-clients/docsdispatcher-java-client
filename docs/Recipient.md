

# Recipient

Specific object to format recipient fields

## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**id** | **String** | Recipient field for internal identifier. Usefull for request scoped references. |  [optional] |
|**firstname** | **String** | Recipient field for person&#39;s firstname |  [optional] |
|**name** | **String** | Recipient field for person&#39;s name or lastname |  [optional] |
|**email** | **String** | Recipient field for person&#39;s email |  [optional] |
|**phone** | **String** | Recipient field for person&#39;s phone number, with international prefix |  [optional] |
|**role** | [**RoleEnum**](#RoleEnum) | Recipient role in the signature process. Either signing party or simple observer |  |
|**targetUsers** | **List&lt;String&gt;** | Email list of targeted users to be reviewed by this recipient  |  [optional] |
|**options** | **Object** |  |  |



## Enum: RoleEnum

| Name | Value |
|---- | -----|
| SIGNER | &quot;SIGNER&quot; |
| APPROVER | &quot;APPROVER&quot; |
| WATCHER | &quot;WATCHER&quot; |



