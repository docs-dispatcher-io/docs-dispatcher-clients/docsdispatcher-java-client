# ConfigurationsHelpersApi

All URIs are relative to *https://api.docs-dispatcher.io*

| Method | HTTP request | Description |
|------------- | ------------- | -------------|
| [**configurationsGetConfiguration**](ConfigurationsHelpersApi.md#configurationsGetConfiguration) | **GET** /api/configurations/{configTypeName}/config | Get the configuration corresponding to the given config type |
| [**configurationsGetConfigurationForProvider**](ConfigurationsHelpersApi.md#configurationsGetConfigurationForProvider) | **GET** /api/configurations/{configTypeName}/providers/{provider}/config | Get the configuration corresponding to the given config type and the given provider |
| [**configurationsList**](ConfigurationsHelpersApi.md#configurationsList) | **GET** /api/configurations | Retrieve all configurations json schema for services that are supported by Dispatcher service |
| [**configurationsListProviders**](ConfigurationsHelpersApi.md#configurationsListProviders) | **GET** /api/configurations/{configTypeName}/providers | Get the list of implemented providers for the given config type |
| [**configurationsValidateConfiguration**](ConfigurationsHelpersApi.md#configurationsValidateConfiguration) | **POST** /api/configurations/{configTypeName}/validate | Validate configuration for the given config type |


<a id="configurationsGetConfiguration"></a>
# **configurationsGetConfiguration**
> List&lt;ConfigurationSchema&gt; configurationsGetConfiguration(configTypeName)

Get the configuration corresponding to the given config type

### Example
```java
// Import classes:
import io.docs_dispatcher.client.ApiClient;
import io.docs_dispatcher.client.ApiException;
import io.docs_dispatcher.client.Configuration;
import io.docs_dispatcher.client.auth.*;
import io.docs_dispatcher.client.models.*;
import io.docs_dispatcher.client.api.ConfigurationsHelpersApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://api.docs-dispatcher.io");
    
    // Configure HTTP bearer authorization: bearerAuth
    HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setBearerToken("BEARER TOKEN");

    ConfigurationsHelpersApi apiInstance = new ConfigurationsHelpersApi(defaultClient);
    Types configTypeName = Types.fromValue("SMTP"); // Types | Config Type for which a configuration is requested
    try {
      List<ConfigurationSchema> result = apiInstance.configurationsGetConfiguration(configTypeName);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ConfigurationsHelpersApi#configurationsGetConfiguration");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **configTypeName** | **Types**| Config Type for which a configuration is requested | [enum: SMTP, SMS, POSTAL, E_SIGN, UPLOAD] |

### Return type

[**List&lt;ConfigurationSchema&gt;**](ConfigurationSchema.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **404** | A distant service response, for ex: Template name unknown, client configuration not found... |  -  |
| **500** | Unexpected error from this service |  -  |
| **200** | Configuration&#39;s json schema |  -  |

<a id="configurationsGetConfigurationForProvider"></a>
# **configurationsGetConfigurationForProvider**
> List&lt;ConfigurationSchema&gt; configurationsGetConfigurationForProvider(configTypeName, provider)

Get the configuration corresponding to the given config type and the given provider

### Example
```java
// Import classes:
import io.docs_dispatcher.client.ApiClient;
import io.docs_dispatcher.client.ApiException;
import io.docs_dispatcher.client.Configuration;
import io.docs_dispatcher.client.auth.*;
import io.docs_dispatcher.client.models.*;
import io.docs_dispatcher.client.api.ConfigurationsHelpersApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://api.docs-dispatcher.io");
    
    // Configure HTTP bearer authorization: bearerAuth
    HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setBearerToken("BEARER TOKEN");

    ConfigurationsHelpersApi apiInstance = new ConfigurationsHelpersApi(defaultClient);
    Types configTypeName = Types.fromValue("SMTP"); // Types | Config Type corresponding to the configuration to validate
    String provider = "provider_example"; // String | The provider name for which the configuration is requested
    try {
      List<ConfigurationSchema> result = apiInstance.configurationsGetConfigurationForProvider(configTypeName, provider);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ConfigurationsHelpersApi#configurationsGetConfigurationForProvider");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **configTypeName** | **Types**| Config Type corresponding to the configuration to validate | [enum: SMTP, SMS, POSTAL, E_SIGN, UPLOAD] |
| **provider** | **String**| The provider name for which the configuration is requested | |

### Return type

[**List&lt;ConfigurationSchema&gt;**](ConfigurationSchema.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **404** | A distant service response, for ex: Template name unknown, client configuration not found... |  -  |
| **500** | Unexpected error from this service |  -  |
| **200** | Configuration&#39;s json schema |  -  |

<a id="configurationsList"></a>
# **configurationsList**
> List&lt;ConfigurationSchema&gt; configurationsList()

Retrieve all configurations json schema for services that are supported by Dispatcher service

### Example
```java
// Import classes:
import io.docs_dispatcher.client.ApiClient;
import io.docs_dispatcher.client.ApiException;
import io.docs_dispatcher.client.Configuration;
import io.docs_dispatcher.client.auth.*;
import io.docs_dispatcher.client.models.*;
import io.docs_dispatcher.client.api.ConfigurationsHelpersApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://api.docs-dispatcher.io");
    
    // Configure HTTP bearer authorization: bearerAuth
    HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setBearerToken("BEARER TOKEN");

    ConfigurationsHelpersApi apiInstance = new ConfigurationsHelpersApi(defaultClient);
    try {
      List<ConfigurationSchema> result = apiInstance.configurationsList();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ConfigurationsHelpersApi#configurationsList");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;ConfigurationSchema&gt;**](ConfigurationSchema.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/plain

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | List of Configuration&#39;s json schemas |  -  |
| **500** | Unexpected error from this service |  -  |

<a id="configurationsListProviders"></a>
# **configurationsListProviders**
> List&lt;String&gt; configurationsListProviders(configTypeName)

Get the list of implemented providers for the given config type

### Example
```java
// Import classes:
import io.docs_dispatcher.client.ApiClient;
import io.docs_dispatcher.client.ApiException;
import io.docs_dispatcher.client.Configuration;
import io.docs_dispatcher.client.auth.*;
import io.docs_dispatcher.client.models.*;
import io.docs_dispatcher.client.api.ConfigurationsHelpersApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://api.docs-dispatcher.io");
    
    // Configure HTTP bearer authorization: bearerAuth
    HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setBearerToken("BEARER TOKEN");

    ConfigurationsHelpersApi apiInstance = new ConfigurationsHelpersApi(defaultClient);
    Types configTypeName = Types.fromValue("SMTP"); // Types | The Config Type for which list of providers is requested
    try {
      List<String> result = apiInstance.configurationsListProviders(configTypeName);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ConfigurationsHelpersApi#configurationsListProviders");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **configTypeName** | **Types**| The Config Type for which list of providers is requested | [enum: SMTP, SMS, POSTAL, E_SIGN, UPLOAD] |

### Return type

**List&lt;String&gt;**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain, application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **404** | A distant service response, for ex: Template name unknown, client configuration not found... |  -  |
| **500** | Unexpected error from this service |  -  |
| **200** | Configuration&#39;s json schema |  -  |

<a id="configurationsValidateConfiguration"></a>
# **configurationsValidateConfiguration**
> Object configurationsValidateConfiguration(configTypeName, configurationElement)

Validate configuration for the given config type

### Example
```java
// Import classes:
import io.docs_dispatcher.client.ApiClient;
import io.docs_dispatcher.client.ApiException;
import io.docs_dispatcher.client.Configuration;
import io.docs_dispatcher.client.auth.*;
import io.docs_dispatcher.client.models.*;
import io.docs_dispatcher.client.api.ConfigurationsHelpersApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("https://api.docs-dispatcher.io");
    
    // Configure HTTP bearer authorization: bearerAuth
    HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
    bearerAuth.setBearerToken("BEARER TOKEN");

    ConfigurationsHelpersApi apiInstance = new ConfigurationsHelpersApi(defaultClient);
    Types configTypeName = Types.fromValue("SMTP"); // Types | Config Type corresponding to the configuration to validate
    ConfigurationElement configurationElement = new ConfigurationElement(); // ConfigurationElement | 
    try {
      Object result = apiInstance.configurationsValidateConfiguration(configTypeName, configurationElement);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling ConfigurationsHelpersApi#configurationsValidateConfiguration");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

| Name | Type | Description  | Notes |
|------------- | ------------- | ------------- | -------------|
| **configTypeName** | **Types**| Config Type corresponding to the configuration to validate | [enum: SMTP, SMS, POSTAL, E_SIGN, UPLOAD] |
| **configurationElement** | [**ConfigurationElement**](ConfigurationElement.md)|  | [optional] |

### Return type

**Object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain, application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **404** | A distant service response, for ex: Template name unknown, client configuration not found... |  -  |
| **417** | For the given service, additional parameters are required and not present |  -  |
| **500** | Unexpected error from this service |  -  |
| **200** | Empty success response |  -  |

