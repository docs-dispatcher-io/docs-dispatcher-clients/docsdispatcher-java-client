

# BasicListRequest


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**targets** | [**List&lt;TargetRequest&gt;**](TargetRequest.md) |  |  [optional] |
|**requests** | [**List&lt;BasicRequest&gt;**](BasicRequest.md) |  |  [optional] |
|**resultFileName** | **String** |  |  [optional] |



