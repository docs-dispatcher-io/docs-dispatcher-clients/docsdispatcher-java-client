

# ESignFileContentRequest

FileContent that could be either a templatable request that has to printed before being used, or a remote file given as URL or a base64 content. A templatable file should have templateName and data properties. A remote file should be passed with resultFileName and url properties. A base64 encoded file should be passed with resultFileName and content properties. 

## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**targets** | [**List&lt;TargetRequest&gt;**](TargetRequest.md) |  |  [optional] |
|**templateName** | **String** |  |  [optional] |
|**resultFileName** | **String** |  |  [optional] |
|**data** | **Object** |  |  [optional] |
|**content** | **String** |  |  [optional] |
|**url** | **String** |  |  [optional] |
|**fields** | [**List&lt;ESignField&gt;**](ESignField.md) |  |  [optional] |



