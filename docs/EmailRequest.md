

# EmailRequest


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**body** | **String** |  |  [optional] |
|**subject** | **String** |  |  [optional] |
|**templateName** | **String** |  |  [optional] |
|**from** | **Object** |  |  [optional] |
|**to** | **List&lt;String&gt;** |  |  [optional] |
|**cc** | **List&lt;String&gt;** |  |  [optional] |
|**bcc** | **List&lt;String&gt;** |  |  [optional] |
|**attachments** | [**List&lt;FileContentRequest&gt;**](FileContentRequest.md) |  |  [optional] |
|**data** | **Object** |  |  [optional] |



