

# MediaFormat

## Enum


* `HTML` (value: `"HTML"`)

* `WORD` (value: `"WORD"`)

* `EXCEL` (value: `"EXCEL"`)

* `POWERPOINT` (value: `"POWERPOINT"`)

* `SVG` (value: `"SVG"`)

* `PNG` (value: `"PNG"`)

* `JPEG` (value: `"JPEG"`)

* `PDF` (value: `"PDF"`)

* `TEXT_PLAIN` (value: `"TEXT_PLAIN"`)

* `MULTIPART_ALTERNATIVE` (value: `"MULTIPART_ALTERNATIVE"`)



