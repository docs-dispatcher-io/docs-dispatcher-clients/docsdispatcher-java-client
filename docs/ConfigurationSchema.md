

# ConfigurationSchema


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**$schema** | **String** |  |  |
|**title** | **String** |  |  |
|**type** | **String** |  |  |
|**additionalProperties** | **Boolean** |  |  |
|**description** | **String** |  |  |
|**properties** | **Object** |  |  |
|**required** | **List&lt;String&gt;** |  |  |
|**configType** | **Types** |  |  |
|**providerName** | **String** |  |  [optional] |



