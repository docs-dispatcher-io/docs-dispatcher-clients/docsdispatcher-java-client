/*
 * Docs Dispatcher API - API document(s) generation
 * Docs.io API enables you to generate files (currently DOCx, XLSx, PPTx, PDF, HTML, JPG/PNG/SVG) from a template and JSON data. You can just download resulting file, send email with generated attachments or compose services.
 *
 * The version of the OpenAPI document: 3.1.1
 * Contact: tech@docs-dispatcher.io
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package io.docs_dispatcher.client.model;

import com.google.gson.annotations.SerializedName;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 * Model tests for MediaFormat
 */
public class MediaFormatTest {
    /**
     * Model tests for MediaFormat
     */
    @Test
    public void testMediaFormat() {
        // TODO: test MediaFormat
    }

}
