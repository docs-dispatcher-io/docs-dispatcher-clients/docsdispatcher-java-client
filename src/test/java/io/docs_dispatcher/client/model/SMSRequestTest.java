/*
 * Docs Dispatcher API - API document(s) generation
 * Docs.io API enables you to generate files (currently DOCx, XLSx, PPTx, PDF, HTML, JPG/PNG/SVG) from a template and JSON data. You can just download resulting file, send email with generated attachments or compose services.
 *
 * The version of the OpenAPI document: 3.1.1
 * Contact: tech@docs-dispatcher.io
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package io.docs_dispatcher.client.model;

import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

/**
 * Model tests for SMSRequest
 */
public class SMSRequestTest {
    private final SMSRequest model = new SMSRequest();

    /**
     * Model tests for SMSRequest
     */
    @Test
    public void testSMSRequest() {
        // TODO: test SMSRequest
    }

    /**
     * Test the property 'providerName'
     */
    @Test
    public void providerNameTest() {
        // TODO: test providerName
    }

    /**
     * Test the property 'smsContent'
     */
    @Test
    public void smsContentTest() {
        // TODO: test smsContent
    }

    /**
     * Test the property 'templateName'
     */
    @Test
    public void templateNameTest() {
        // TODO: test templateName
    }

    /**
     * Test the property 'from'
     */
    @Test
    public void fromTest() {
        // TODO: test from
    }

    /**
     * Test the property 'to'
     */
    @Test
    public void toTest() {
        // TODO: test to
    }

    /**
     * Test the property 'data'
     */
    @Test
    public void dataTest() {
        // TODO: test data
    }

}
